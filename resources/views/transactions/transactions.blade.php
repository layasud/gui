@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                @foreach ($transactions as $transaction)
                    <div class="m-b-sm">
                        <div class="row">
                            <div class="col-xs-6">
                                Transaction amount
                            </div>
                            <div class="col-xs-6">
                                ${{ number_format($transaction['amount'],2) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Transaction date
                            </div>
                            <div class="col-xs-6">
                                {{ $transaction['date'] }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Transaction author
                            </div>
                            <div class="col-xs-6">
                                {{ $transaction['name'] }}
                            </div>
                        </div>
                    </div>
                @endforeach

                <div>
                    {{ $transactions->links() }}
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div>
                    Search Transactions.
                </div>
                {{ Form::open(['url' => 'transactions','method'=>'GET']) }}
                <div class="form-group">
                    {{ Form::Label('uses', 'User:') }}
                    {{ Form::select('user_id', $users->pluck('name','id'), null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::Label('date_from', 'Date From:') }}
                    {{ Form::date('date_from', \Carbon\Carbon::now()->addDay(-1)) }}
                </div>
                <div class="form-group">
                    {{ Form::Label('date_to', 'Date To:') }}
                    {{ Form::date('date_to', \Carbon\Carbon::now()->addDay(1)) }}
                </div>
                <div class="form-group">
                    {{ Form::Label('amount_from', 'Amount from:') }}
                    {{ Form::input('number', 'amount_from', 1, ['class' => 'form-control','min'=>1]) }}
                </div>
                <div class="form-group">
                    {{ Form::Label('amount_to', 'Amount to:') }}
                    {{ Form::input('number', 'amount_to', 100, ['class' => 'form-control','min'=>1]) }}
                </div>
                {{  Form::submit('Search!') }}
                {{ Form::close() }}
            </div>
        </div>

    </div>
@endsection
