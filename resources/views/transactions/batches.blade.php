@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                @foreach ($batches as $batch)
                    <div class="m-b-sm">
                        <div class="row">
                            <div class="col-xs-6">
                                Batch amount
                            </div>
                            <div class="col-xs-6">
                                ${{ number_format($batch['amount'],2) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Transactions from
                            </div>
                            <div class="col-xs-6">
                                {{ $batch['from'] }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Transactions to
                            </div>
                            <div class="col-xs-6">
                                {{ $batch['to'] }}
                            </div>
                        </div>
                    </div>
                @endforeach

                <div>
                    {{ $batches->links() }}
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div>
                    Search Transaction Batches.
                </div>
                {{ Form::open(['url' => 'batches','method'=>'GET']) }}
                <div class="form-group">
                    {{ Form::Label('date_from', 'Date From:') }}
                    {{ Form::date('date_from', \Carbon\Carbon::now()->addDay(-1)) }}
                </div>
                <div class="form-group">
                    {{ Form::Label('date_to', 'Date To:') }}
                    {{ Form::date('date_to', \Carbon\Carbon::now()->addDay(1)) }}
                </div>
                {{  Form::submit('Search!') }}
                {{ Form::close() }}
            </div>
        </div>

    </div>
@endsection
