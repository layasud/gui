@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Transaction</div>

                    <div class="panel-body">
                        {{ Form::open(['url' => 'transaction-create']) }}
                        <div class="form-group">
                            {{ Form::Label('amount', 'Amount:') }}
                            {{ Form::input('number', 'amount', 100, ['class' => 'form-control','min'=>1]) }}
                        </div>
                        {{  Form::submit('Submit') }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
