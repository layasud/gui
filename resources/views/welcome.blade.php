@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                iSoftBet Remote PHP Test
            </div>

            <div class="title-sm">
                Candidate
            </div>

            <div class="links center">
                <a href="https://ua.linkedin.com/in/alexglotka">Alex Glotka</a>
            </div>
        </div>
    </div>
@endsection
