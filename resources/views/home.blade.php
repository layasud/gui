@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You can view <a href="{{ route('transactions') }}">transactions</a>, <a href="{{ route('transaction-create') }}">create</a> one or view transaction <a href="{{ route('batches') }}">batches</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
