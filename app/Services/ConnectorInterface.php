<?php

namespace App\Services;
/**
 * Interface describes standard micriservice connector
 * Interface ConnectorInterface
 * @package App\Services
 */
Interface ConnectorInterface
{
    public function makeRequest($requestType,$method,$options = []);
}
