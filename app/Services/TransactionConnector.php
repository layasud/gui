<?php

namespace App\Services;

class TransactionConnector implements ConnectorInterface
{
    protected $client;
    protected $host;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client;
        $this->host = env('TRANSAPI');
    }

    public function makeRequest($requestType,$method,$options = [])
    {
        return $this->$requestType($method,$options);
    }

    protected function transaction($method,$options = [])
    {
        $uri = env('TRANSAPI') . '/transactions';
        $response = $this->client->request($method,$uri,$options);
        if ( !$response->getStatusCode() ) {
            abort(500);
        }
        return json_decode($response->getBody(),true);
    }

    protected function batch($method,$options = [])
    {
        $uri = env('TRANSAPI') . '/batches';
        $response = $this->client->request($method,$uri,$options);
        if ( !$response->getStatusCode() ) {
            abort(500);
        }
        return json_decode($response->getBody(),true);
    }
}
