<?php

namespace App\Http\Controllers\Transactions;

use App\Services\TransactionConnector;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;

class BatchController extends Controller
{
    protected $connector;

    public function __construct()
    {
        $this->connector = new TransactionConnector();
    }

    public function index(Request $request)
    {
        $response = $this->connector->makeRequest('batch','GET',['query'=>$request->all()]);

        // create paginator
        $data = new LengthAwarePaginator(
            $response['data'],
            $response['total'],
            $response['per_page'],
            $response['current_page']
        );
        $data->withPath('batches');
        return view('transactions.batches',['batches'=>$data]);
    }
}
