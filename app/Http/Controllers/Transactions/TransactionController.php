<?php

namespace App\Http\Controllers\Transactions;

use App\Services\TransactionConnector;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;

/**
 * controller connects views and api
 * Class TransactionController
 * @package App\Http\Controllers\Transactions
 */
class TransactionController extends Controller
{
    /**
     * api connector
     * @var TransactionConnector
     */
    protected $connector;

    public function __construct()
    {
        $this->connector = new TransactionConnector();
    }

    /**
     * request api with a given parameters
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $response = $this->connector->makeRequest('transaction','GET',['query'=>$request->all()]);
        // get all users for request info enhancement and search
        $users = User::all();
        // append user info to transactions
        foreach ($response['data'] as &$r) {
            $user = $users->filter(function($u)use($r){
                return $r['user_id'] === $u->getKey();
            })->first();
            $r['name'] = $user->name;
        }
        // create paginator
        $data = new LengthAwarePaginator(
            $response['data'],
            $response['total'],
            $response['per_page'],
            $response['current_page']
        );
        $data->withPath('transactions');
        return view('transactions.transactions',['transactions'=>$data,'users'=>$users]);
    }
}
