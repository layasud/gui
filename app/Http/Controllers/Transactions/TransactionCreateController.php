<?php

namespace App\Http\Controllers\Transactions;

use App\Services\TransactionConnector;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class TransactionCreateController extends Controller
{
    protected $connector;

    public function __construct()
    {
        $this->connector = new TransactionConnector();
    }

    public function index(Request $request)
    {
        return view('transactions.transaction-create');
    }

    public function store(Request $request)
    {
        $amount = $request->input('amount');
        $user = \Auth::user();
        $data = [
            'user_id' => $user->getKey(),
            'amount' => $amount,
            'date' => Carbon::now(),
        ];
        $this->connector->makeRequest('transaction','POST',['form_params'=>$data]);
        return redirect('transactions');
    }
}
