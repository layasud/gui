<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace'=>'Transactions','middleware'=>'auth'],function(){
    Route::get('/transactions', 'TransactionController@index')->name('transactions');
    Route::get('/transaction-create', 'TransactionCreateController@index')->name('transaction-create');
    Route::post('/transaction-create', 'TransactionCreateController@store');
    Route::get('/batches', 'BatchController@index')->name('batches');
});
